#pragma comment(lib, "..\\x64\\Debug\\library.lib")
#include "..\\library\\FileStream.h"
#include "..\\library\\OutStream.h"

int main()
{
	msl::OutStream one;
	one << ("I am the Doctor and I'm ") << (1500) << (" years old") << (msl::endline);
	msl::FileStream f;
	f << ("I am the Doctor and I'm ") << (1500) << (" years old") << (msl::endline);
	return 0;
}