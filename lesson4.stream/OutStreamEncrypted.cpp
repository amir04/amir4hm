#include "OutStreamEncrypted.h"

#pragma warning (disable:4996)

OutStreamEncrypted::OutStreamEncrypted(unsigned int key)
{
	this->key = key;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(char* str)
{
	int i = 0;
	char c = 0;
	for (i; i < strlen(str); i++)
	{
		c = str[i];
		if (c >= 32 && c <= 126)
		{
			c = char((int(c + this->key - 32) % 95) + 32);
		}
		printf("%c", c);
	}
	return *this;

}

OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	char buffer[100] = { 0 };
	OutStreamEncrypted o = OutStreamEncrypted(this->key);
	sprintf(buffer, "%d", num);
	o << buffer;
	return *this;

}
OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)())
{
	pf();
	return *this;
}