#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"
int main(int argc, char **argv)
{
	OutStream o;
	o << ("I am the Doctor and I'm ") << (1500) << (" years old") << (endline);
	FileStream f;
	f << ("I am the Doctor and I'm ") << (1500) << (" years old") << (endline);
	OutStreamEncrypted ceacerCode(3);
	ceacerCode << ("I am the Doctor and I'm ") << (1500) << (" years old") << (endline);
	Logger first;
	first << ("I am the Doctor and I'm ") << (1500) << (" years old") << (endline);
	Logger second;
	second << ("I am the Doctor and I'm ") << (1500) << (" years old") << (endline);

	getchar();
	return 0;
}
