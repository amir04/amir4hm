#include "Logger.h"

Logger::Logger()
{
	_startLine = true;
}

Logger::~Logger()
{
}

Logger& operator<<(Logger& l, const char *msg)
{
	if (l._startLine)
	{
		printf("LOG: %s", msg);
		l._startLine = false;
	}
	else
	{
		printf("%s", msg);
	}
	return l;
}

Logger& operator<<(Logger& l, int num)
{
	if (l._startLine)
	{
		printf("LOG: %d", num);
		l._startLine = false;
	}
	else
	{
		printf("%d", num);
	}
	return l;
}

Logger& operator<<(Logger& l, void(*pf)())
{
	endline();
	l.setStartLine();
	l._startLine = true;
	return l;
}

void Logger::setStartLine()
{
	unsigned int static lineCounter = 0;
	lineCounter++;
	printf("LOG: %d\n", lineCounter);
}