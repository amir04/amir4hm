#include "FileStream.h"

#pragma warning (disable:4996)

//class FileStream:
FileStream::FileStream()
{
	_file = fopen("file.txt", "w");
}

FileStream::~FileStream()
{
	if (_file)
	{
		fclose(_file);
	}
}

FileStream& FileStream::operator<<(const char *str)
{
	if (_file)
	{
		fprintf(_file, "%s", str, stdin);
	}
	return *this;
}

FileStream& FileStream::operator<<(int num)
{
	if (_file)
	{
		fprintf(_file, "%d", num, stdin);
	}
	return *this;
}

FileStream& FileStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}


void endline(FILE * file)
{
	if (file)
	{
		fprintf(file, "\n", stdin);
	}
}

