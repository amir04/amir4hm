#pragma once

#include "OutStream.h"

#include <string>
#include <stdlib.h>

class OutStreamEncrypted : public OutStream
{
private:
	unsigned int key;
public:
	OutStreamEncrypted(unsigned int key);
	//i dont need  to create destractor, i use the implemenation of OutStream
	OutStreamEncrypted& operator<<(char* str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)());
};
