#pragma once
#include "OutStream.h"
#include <fstream>
class FileStream : public OutStream
{
public:
	FileStream();
	~FileStream();
	FileStream& operator<<(const char *str);
	FileStream& operator<<(int num);
	FileStream& operator<<(void(*pf)());
};
void endline(FILE * file);
